<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>test app PWA</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">

    <iframe src="station_01.svg" id = "app-demo" width="100%" height = "650px" onload="app()"></iframe>
    <!-- load the d3.js library -->	
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.min.js"></script>
    <script src="js/jquery-2.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/date.js"></script>
    <script src="dma-app/app.js"></script>
    <script src="js/moment/min/moment.min.js"></script>
    <script type="text/javascript">

        let configjson = new WebSocket("ws://iotdma.info:1880/pwa/sensor");

        function clicked(evt){
            alert("x: "+evt.clientX+" y:"+evt.clientY);
        }

        $(document).ready(function(){


            $("#app-demo").load(function(){
                // $(this).contents().on("mousemove", function(){
                //     showCoords(event)
                // });
                
                // console.log($("svg"))
                var iframe_1 = document.getElementById("app-demo")
                var elems = iframe_1.contentWindow.document.getElementsByTagName("svg")
                elems[0].children[1].setAttribute("id", "g-content");
                // $("#g-content").append('<text x="20" y="20" font-family="sans-serif" font-size="20px" fill="red">VALUE</text>');
                $(this).contents().on('click', 'svg', function(){
                    showCoords(event)
                });

            });






            configjson.onopen = function(e) {
                console.log("[open] Connection established, send -> server");
            };

            configjson.onmessage = function(event) {
                var obj = JSON.parse(event.data);
                // console.log(obj);
                try {
                    if (obj.device_id == "5541025_PTE_RWP"){
                        for (var i = 0;i < obj.data.length;i++){
                            if (obj.data[i].type == "value"){
                                var iframe_2 = document.getElementById("app-demo")
                                x = iframe_2.contentWindow.document.getElementById(obj.data[i].tag)
                                x.textContent = obj.data[i].value;
                            }else if (obj.data[i].type == "status"){
                                var iframe_2 = document.getElementById("app-demo")
                                if (obj.data[i].value == "1"){
                                    y = iframe_2.contentWindow.document.getElementById(obj.data[i].tag)
                                    y.setAttribute("fill","#ff0000")

                                }else {
                                    y = iframe_2.contentWindow.document.getElementById(obj.data[i].tag)
                                    y.setAttribute("fill","#ffffff")

                                }
                            }else if (obj.data[i].type == "date"){
                                var dd = moment(obj.data[i].value).format('YYYY-MM-DD HH:mm:ss');
                                var iframe_2 = document.getElementById("app-demo")
                                x = iframe_2.contentWindow.document.getElementById(obj.data[i].tag)
                                x.textContent = dd;
                            }
                        }
                    }   
                } catch (error) {
                    console.log(error)
                }
            };

            configjson.onclose = function(event) {
                if (event.wasClean) {
                    // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
                } else {
                    // alert('[close] Connection died');
                }
            };

            configjson.onerror = function(error) {
                // alert(`[error] ${error.message}`);
            };
        });



        /*
        
        JSON

        {
            "device_id": "5541025_PTE_RWP",
            "data":[
                {
                    "tag": "vr1",
                    "value": "55.2",
                    "type": "value"
                },{
                    "tag": "vr2",
                    "value": "14.6",
                    "type": "value"
                },{
                    "tag": "status1",
                    "value":"1",
                    "type": "status"
                }
            ]
        }
        
        
        */
        // function onloadiframe(){
        //     app();
        // }
        function showCoords(event) {
            var iframe_1 = document.getElementById("app-demo")
            var elems = iframe_1.contentWindow.document.getElementsByTagName("svg")
            var x = event.clientX;
            var y = event.clientY;
            var coords = "X coords: " + x + ", Y coords: " + y;
            // var holder = d3.select(elems).append("text")
            //     .attr("x", x)
            //     .attr("y", y)
            //     .style("fill", "black")
            //     .style("font-size", "20px")
            //     .attr("dy", ".35em")
            //     .attr("text-anchor", "middle")
            //     .style("pointer-events", "none")
            //     .text(coords);


                
            // $("#g-content").append('<text x="'+x+'" y="'+y+'" font-family="sans-serif" font-size="20px" fill="red">'++'</text>');
            console.log(coords);
            var circle= makeSVG('text', {x: x, y: y + 3, stroke: 'black', 'stroke-width': 2, fill: 'red', id : "vr2"});
            // document.getElementById('#g-content').appendChild(circle);
            console.log(circle);
            var iframe_1 = document.getElementById("app-demo")
            var elems = iframe_1.contentWindow.document.getElementsByTagName("svg")
            elems[0].children[1].appendChild(circle);
                
        }
        function makeSVG(tag, attrs) {

            var el= document.createElementNS('http://www.w3.org/2000/svg', tag);
            for (var k in attrs)
                el.setAttribute(k, attrs[k]);
            el.textContent = "-"
            return el;
        }
    </script>

</body>
</html>
